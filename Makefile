
CC = gcc
CFLAGS = -g -Wall -O3
OMPFLAGS = -fopenmp
RM = rm -f

all: ex04

%.o: %.c
	$(CC) $(CFLAGS) $(OMPFLAGS) -c -o $@ $<

ex04.o: forces.h
forces.o: forces.h

ex04: ex04.o forces.o
	$(CC) $(OMPFLAGS) -o $@ $?

clean:
	$(RM) -f *.o ex04
