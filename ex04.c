#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "forces.h"

#if !defined(N)
#define N 10000
#endif

static double posstart[3 * N];
static double pos[3 * N];
static double forces[3 *N];

int main(int argc, char **argv)
{
  int    i;
  int    Nsteps = 5000;
  int    Ncheck = 500;
  double M      = 1.;
  double dt     = 1.e-4;

  if (argc > 1) Nsteps = atoi(argv[1]);
  if (argc > 2) Ncheck = atoi(argv[2]);

  /* initialize randomly */
  for (i = 0; i < 3 * N; i++) {
    pos[i] = posstart[i] = (double) rand() / (double) RAND_MAX;
  }

  for (i = 0; i < Nsteps; i++) {
    int j;

    if (!(i % Ncheck)) {
      double avg_distance = 0.;

      /* TODO: compute average distance of pos from posstart */
      printf("Average distance after %d timesteps: %e\n", i, avg_distance);
    }

    compute_forces(N, pos, forces);
    for (j = 0; j < 3 * N; j++) {
      /* Hmm, this is wrong, isn't it?  This will create noise that's
       * uniformly distributed in [0,1], not [-1, 1].  If you're using this
       * for your assignment, you'll have to fix this definition of noise. */
      double noise = (double) rand() / (double) RAND_MAX;

      pos[j] += M * forces[i] + sqrt(2. * dt) * noise;
    }
  }

  if (!(i % Ncheck)) {
    double avg_distance = 0.;
    /* TODO: compute average distance of pos from posstart */
    printf("Average distance after %d timesteps: %e\n", i, avg_distance);
  }

  return 0;
}
